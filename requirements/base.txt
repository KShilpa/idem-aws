boto3>=1.20.24
configparser
idem==18.7.0
pgpy>=0.5.4
deepdiff==5.8.0
heist>=6.1.0
pycryptodomex
