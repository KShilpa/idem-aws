def test_auto_scaling_group_utils(hub):

    # When current state is empty and desired state is not empty. is auto scaling updated should be true
    current_state = {}
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = (
        hub.tool.aws.autoscaling.auto_scaling_group_utils.is_auto_scaling_group_updated(
            current_state, desired_state
        )
    )
    assert result

    # When current state is same as desired state and tags are not same. is auto scaling updated should be false
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = (
        hub.tool.aws.autoscaling.auto_scaling_group_utils.is_auto_scaling_group_updated(
            current_state, desired_state
        )
    )
    assert not result

    # When current state and desired state  are not same. is auto scaling updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 3,
        "max_size": 5,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = (
        hub.tool.aws.autoscaling.auto_scaling_group_utils.is_auto_scaling_group_updated(
            current_state, desired_state
        )
    )
    assert result

    # When desired state contains a new non-empty parameter. is auto scaling updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_template": "lt-3",
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    result = (
        hub.tool.aws.autoscaling.auto_scaling_group_utils.is_auto_scaling_group_updated(
            current_state, desired_state
        )
    )
    assert result
