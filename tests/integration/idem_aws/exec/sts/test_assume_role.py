async def test_credentials(hub, ctx, aws_iam_role):
    role_arn = aws_iam_role["arn"]
    role_session_name = "idem-aws_test_session"
    ret = await hub.exec.aws.sts.assume_role.credentials(
        ctx, role_arn=role_arn, role_session_name=role_session_name
    )
    assert ret.result, ret.comment
    # Verify that all the values were properly retrieved for the named session
    assert ret.ret["AccessKeyId"]
    assert ret.ret["Expiration"]
    assert ret.ret["SecretAccessKey"]
    assert ret.ret["SessionToken"]


async def test_bad_credentials(hub, ctx):
    role_arn = "malformed_arn"
    role_session_name = "malformed_session_name"
    ret = await hub.exec.aws.sts.assume_role.credentials(
        ctx, role_arn=role_arn, role_session_name=role_session_name
    )
    assert not ret.result, ret.comment
    assert "ParamValidationError" in ret.comment[0]
